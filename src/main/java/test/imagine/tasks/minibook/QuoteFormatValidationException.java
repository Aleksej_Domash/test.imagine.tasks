package test.imagine.tasks.minibook;

public class QuoteFormatValidationException extends RuntimeException {
    public QuoteFormatValidationException(String message) {
        super(message);
    }
}
