package test.imagine.tasks.minibook.impl;

import test.imagine.tasks.minibook.Quote;

import java.math.BigDecimal;

public class QuoteImpl implements Quote {
    private final String id;
    private BigDecimal price;
    private long volume;
    private long timestamp;

    public QuoteImpl(final String id) {
        this.id = id;
    }


    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public long getVolume() {
        return volume;
    }

    public String getId() {
        return id;
    }


    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return id + "/" + price + "/" + volume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuoteImpl quote = (QuoteImpl) o;

        return id.equals(quote.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
