package test.imagine.tasks.animate;

public class DefaultAnimationRenderer implements Animation.AnimationRenderer {
    @Override
    public char[] render(char[] buffer) {
        char convertedAnimation [] = new char [buffer.length];
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] != Animation.EMPTY) {
                convertedAnimation[i] = 'X';
            } else {
                convertedAnimation[i] = buffer[i];
            }
        }
        return convertedAnimation;
    }
}
