package test.imagine.tasks.minibook;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

public class MiniBookTest {

    private static final Logger LOGGER = Logger.getLogger(MiniBookTest.class.getName());

    private static final String[] TEST_DUMP_RESULT_OFFERS_SEQUENCE = {
            "Q1/1.31/1000000",
            "Q6/1.32/1000000"
    };

    private static final String[] TEST_DUMP_RESULT_BIDS_SEQUENCE = {
            "Q3/1.22/1000000",
            "Q2/1.21/1000000",
            "Q4/1.20/1000000",
            "Q5/1.20/500000"
    };

    private static final String[] TEST_DUMP_INPUT_SEQUENCE = {
            "Q1/O/N/1.31/1000000",
            "Q2/B/N/1.21/1000000",
            "Q3/B/N/1.22/1000000",
            "Q4/B/N/1.20/1000000",
            "Q5/B/N/1.20/1000000",
            "Q6/O/N/1.32/1000000",
            "Q7/O/N/1.33/200000",
            "Q5/B/U/1.20/500000",
            "Q7/O/U/1.33/100000",
            "Q7/O/D/0/0"
    };

    @Test
    public void testReceive() {
        MiniBook miniBook = MiniBookFactory.createMiniBook();
        assertNotNull("minibook should have implementation", miniBook);
        Stream.of(TEST_DUMP_INPUT_SEQUENCE).forEach(miniBook::receive);

        Map<Quote.QuoteType, List<Quote>> dump = miniBook.dump();
        assertNotNull("dump can't be null", dump);

        List<String> offersInDump = new ArrayList<>();
        dump.get(Quote.QuoteType.OFFER).stream().forEach((quote) -> {
            offersInDump.add(quote.toString());
        });
        LOGGER.info("OFFER\n" + offersInDump.toString());
        assertArrayEquals("Dump of offers should have proper sequence", TEST_DUMP_RESULT_OFFERS_SEQUENCE, offersInDump.toArray(new String[0]));

        List<String> bidsInDump = new ArrayList<>();
        dump.get(Quote.QuoteType.BID).stream().forEach((quote) -> {
            bidsInDump.add(quote.toString());
        });
        LOGGER.info("BIDS\n" + bidsInDump.toString());
        assertArrayEquals("Dump of bids should have proper sequence", TEST_DUMP_RESULT_BIDS_SEQUENCE, bidsInDump.toArray(new String[0]));
    }

}
